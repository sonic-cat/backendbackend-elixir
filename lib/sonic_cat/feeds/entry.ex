defmodule SonicCat.Feeds.Entry do
  use Ecto.Schema

  import Ecto.Changeset

  alias SonicCat.Feeds.Feed
  alias SonicCat.Feeds.Entry

  @timestamps_opts [usec: false] # Disables microsecond precision
  @derive {Poison.Encoder, only: [:id, :title, :url, :date, :feed_id]}
  schema "feeds_entries" do
    field :title, :string
    field :url, :string
    field :date, :utc_datetime
    belongs_to :feed, Feed

    timestamps()
  end

  @doc false
  def changeset(%Entry{} = entry, attrs \\ %{}) do
    entry
    |> cast(attrs, [:title, :url, :date, :feed_id])
    |> validate_required([:title, :url, :date])
  end
end
