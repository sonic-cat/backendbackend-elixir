defmodule SonicCat.Feeds.Feed do
  use Ecto.Schema

  import Ecto.Changeset

  alias SonicCat.Feeds.{Entry, Feed}
  alias SonicCat.Accounts.User

  @timestamps_opts [usec: false] # Disables microsecond precision
  @derive {Poison.Encoder, only: [:id, :author, :description, :image, :name, :url, :user_id, :entries]}
  schema "feeds_feeds" do
    field :author, :string
    field :description, :string
    field :image, :string
    field :name, :string
    field :url, :string
    field :hash, :string
    belongs_to :user, User
    has_many :entries, Entry, on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(%Feed{} = feed, attrs) do
    feed
    |> cast(attrs, [:name, :image, :author, :description, :url, :hash, :user_id])
    |> cast_assoc(:entries)
    |> validate_required([:name, :image, :author, :url, :hash])
  end
end
