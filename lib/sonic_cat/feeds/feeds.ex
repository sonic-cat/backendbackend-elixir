defmodule SonicCat.Feeds do
  @moduledoc """
  The boundary for the Feeds system.
  """

  import Ecto.Query, warn: false
  import SweetXml

  alias SonicCat.Repo
  alias SonicCat.Feeds.Feed
  alias SonicCat.Feeds.Entry

  def list_feeds(user) do
    Feed
    |> where([u], u.user_id == ^user.id)
    |> order_by(:name)
    |> Repo.all()
    |> Repo.preload([:user, :entries])
  end

  def get_feed!(id) do
    feed =
      Feed
      |> Repo.get!(id)
      |> Repo.preload([:user, entries: from(e in Entry, order_by: [desc: e.date])])

    with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(feed.url) do
      hash = :crypto.hash(:md5, body) |> Base.encode16()
      cond do
        hash != feed.hash ->
          feed
          |> Feed.changeset(parseFeed(body))
          |> Repo.update!()
        true -> feed
      end
    end
  end

  def create_feed(%{"url" => url, "user_id" => user_id} = attrs \\ %{}) do
    case Repo.get_by(Feed, %{url: url, user_id: user_id}) do
      nil ->
        with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(url) do
          feed_data =
            parseFeed(body)
            |> Map.merge(attrs)

          %Feed{}
          |> Feed.changeset(feed_data)
          |> Repo.insert()
        else
          _err -> {:error, "Invalid URL"}
        end
      feed ->
        {:ok, feed}
    end
  end

  def delete_feed(%Feed{} = feed) do
    Repo.delete(feed)
  end

  def change_feed(%Feed{} = feed) do
    Feed.changeset(feed, %{})
  end

  defp parseFeed(body) do
    hash = :crypto.hash(:md5, body) |> Base.encode16()
    result =
      body
      |> xmap(
        name: ~x"//rss/channel/title/text()"s,
        image: ~x"//rss/channel/itunes:image/@href"s,
        author: ~x"//rss/channel/itunes:author/text()"s,
        description: ~x"//rss/channel/description/text()"s,
        entries: [
          ~x"//rss/channel/item"l,
          title: ~x"./title/text()"s,
          url: ~x"./enclosure/@url"s,
          date: ~x"./pubDate/text()"s,
        ]
      )
      |> Enum.reduce(%{}, fn({key, val}, acc) -> Map.put(acc, Atom.to_string(key), val) end)
      |> Map.put("hash", hash)

    attrs = Map.put(result, "entries", Enum.map(Map.get(result, "entries"), fn (entry) ->
      entry = case Timex.parse(Map.get(entry, :date), "{RFC1123}") do
        {:ok, date} -> Map.put(entry, :date, date)
        {:error, _} -> Map.put(entry, :date, Timex.parse!(Map.get(entry, :date), "%a, %d %b %Y %k:%M:%S %z", :strftime))
      end

      Enum.reduce(entry, %{}, fn({key, val}, acc) -> Map.put(acc, Atom.to_string(key), val) end)
    end))

    Map.put(attrs, "entries", Enum.filter(Map.get(attrs, "entries"), fn (entry) ->
      Entry.changeset(%Entry{}, entry).valid?
    end))
  end
end
