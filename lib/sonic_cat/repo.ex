defmodule SonicCat.Repo do
  use Ecto.Repo, otp_app: :sonic_cat

  @doc """
  Dynamically loads the repository info from environment variables.
  """
  def init(_, opts) do
    IO.inspect(Application.get_env(:sonic_cat, :environment), label: "env")
    IO.inspect(System.get_env("DB_HOST"), label: "DB_HOST")
    IO.inspect(System.get_env("DB_NAME"), label: "DB_NAME")
    IO.inspect(System.get_env("DB_USER"), label: "DB_USER")
    IO.inspect(System.get_env("DB_PASSWORD"), label: "DB_PASSWORD")
    opts =
      case Application.get_env(:sonic_cat, :environment) do
        :prod ->
          opts
          |> Keyword.put(:hostname, System.get_env("DB_HOST"))
          |> Keyword.put(:database, System.get_env("DB_NAME"))
          |> Keyword.put(:username, System.get_env("DB_USER"))
          |> Keyword.put(:password, System.get_env("DB_PASSWORD"))
        _other -> opts
      end
    {:ok, opts}
  end
end
